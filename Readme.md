## Synopsis

This repository is a collection of useful material provided by students for **CSE237C** course at UC San Diego.

## Submissions

**Submitting here is completely optional. Do not submit course homeworks or projects.**

But if you submit, please provide a brief explanation for your scripts, document, etc. Let's have an organized repository. Put your material in separate folders. A well commented code is always appreciated!

## License

Open source and free!
