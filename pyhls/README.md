### Overview
These scripts are intended to automate the process of creating tables and plots for comparing by parsing the the xml report outputs from Vivado HLS CSynth. It does this by searching for the xml reports given a set of directories.

### Installation
$ python setup.py build install --user

### Usage
Assuming the following subdirectories: fir8_optimized1 fir8_optimized2    fir8_optimized3    fir8_best  fir128_baseline     fir128_optimized1   fir128_best

$ report2table.py --dirs fir8_best fir8_optimized1 fir8_optimized2

Alternatively if the toplevel function is not the first part of the directory name which is often the case.

$ report2table.py --toplevel fir --dirs fir8_best fir8_optimized1 fir8_optimized2
