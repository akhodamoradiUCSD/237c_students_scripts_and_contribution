# The MIT License (MIT)
# Copyright (c) 2016 Matt Epperson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

#!/usr/bin/env python

import os, sys, glob, argparse, re
import xml.etree.ElementTree as xml
import matplotlib.pyplot as plt

from pylatex import tabulate

def parse_xml(path):
    '''
    Given a path to a report.xml file this function will find and return
    the values for the number of resources used in that solution
    in the following order BRAM_18K, DSP48E, FF, LUT as well as the throughput
    calculated in Million operations per second.
    '''
    clock_rates = {}
    clock_rates['ns'] = 1e-9
    clock_rates['us'] = 1e-6
    clock_rates['ms'] = 1e-3
    clock_rates['s']  = 1

    # Parse XML to get fields we care about. Note their xml heirachy is a little screwy
    tree = xml.parse(path)
    area         = tree.getroot().find("AreaEstimates").find("Resources")
    clock        = tree.getroot().find("PerformanceEstimates").find("SummaryOfTimingAnalysis").find("EstimatedClockPeriod").text
    clock_unit   = tree.getroot().find("PerformanceEstimates").find("SummaryOfTimingAnalysis").find("unit").text
    interval_max = tree.getroot().find("PerformanceEstimates").find("SummaryOfOverallLatency").find("Interval-max").text
    interval_min = tree.getroot().find("PerformanceEstimates").find("SummaryOfOverallLatency").find("Interval-min").text

    # Calculate throughput
    throughput_max = 1e-6 / (float(clock) * float(interval_max) * \
        clock_rates[clock_unit])
    throughput_min = 1e-6 / (float(clock) * float(interval_min) * \
        clock_rates[clock_unit])

    tmax = "\[Throughput_{max} = \\frac{1}{"+clock+" "+clock_unit+" * "+interval_max+" cycles}"\
           +"*\\frac{1 Mhz}{1e6 Hz} = "+str(throughput_max)+" Mhz\]"
    tmin = "\[Throughput_{min} = \\frac{1}{"+clock+" "+clock_unit+" * "+interval_min+" cycles}"\
           +"*\\frac{1 Mhz}{1e6 Hz} = "+str(throughput_min)+" Mhz\]"

    # Make sure pair values are always sorted in the same way
    resources = [[str(child.tag), str(child.text)] for child in area]
    resources = [resource[1] for resource in sorted(resources, key=lambda x: x[0])]

    return resources, tmax, tmin

def get_directories_prefix(prefix):
    paths = []
    for path in glob.glob(prefix+"*/*/solution*/syn/report/"):
        for path,dirs,files in os.walk(path):
            for filename in files:
                paths.append(os.path.join(path,filename))

    return [path for path in paths if "/"+args.prefix+"_csynth.xml" in path]

def get_directories_dirs(dirs, toplevel=""):

    if isinstance(dirs, basestring):
        dirs = [args.dirs]

    paths = []
    for d in dirs:
        for path in glob.glob(d+"/*/*/syn/report/"):
            for path,dirs,files in os.walk(path):
                for filename in files:
                    paths.append(os.path.join(path,filename))


    paths = [path for path in paths if "/"+toplevel+"_csynth.xml" in path]
    return paths
