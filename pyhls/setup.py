from distutils.core import setup
setup(name='pyHLS',
      version='1.0',

      description='A sample Python project',
      long_description='long_description',

      # Author details
      author='Matt Epperson',
      author_email='m.p.epperson@gmail.com',

      # Choose your license
      license='MIT',

      py_modules=['report2table'],
      scripts=['report2table.py', 'pylatex.py', 'pyhls.py']
      )
